import { Transactional } from '@aspectjs/persistence';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}
  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async create(user: User) {
    await this.usersRepository.save(user);
  }
  @Transactional()
  async createAll(users: User[]) {
    for (const u of users) {
      await this.usersRepository.save(u);
    }
  }
  async deleteAll() {
    await this.usersRepository.remove(await this.usersRepository.find());
  }
}
