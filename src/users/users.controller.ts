import { Body, Controller, Delete, Get, Post } from '@nestjs/common';
import { User } from '../user.entity';
import { UsersService } from './users.service';

@Controller('/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  async getHello(): Promise<User[]> {
    return this.usersService.findAll();
  }
  @Post()
  async createNewUser(@Body() user: User): Promise<void> {
    await this.usersService.create(user);
  }

  @Delete('all')
  async deteleAllUser(): Promise<void> {
    await this.usersService.deleteAll();
  }
  @Post('all')
  async createAllUser(@Body() users: User[]): Promise<void> {
    await this.usersService.createAll(users);
  }
}
