import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Post {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  content?: string = 'Hello';

  @ManyToOne(() => User, (user) => user.posts)
  user?: User;
}
