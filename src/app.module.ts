import { getWeaver } from '@aspectjs/core';
import { TypeOrmTransactionalAspect } from '@aspectjs/persistence/typeorm';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Post } from './post.entity';
import { User } from './user.entity';
import { UsersModule } from './users/users.module';
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory() {
        return {
          type: 'postgres',
          host: 'localhost',
          port: 5432,
          username: 'user',
          password: 'password',
          database: 'cdb',
          entities: [User, Post],
          synchronize: true,
        };
      },
      async dataSourceFactory(options) {
        const ds = new DataSource(options!);
        getWeaver().enable(new TypeOrmTransactionalAspect().configure(ds));
        return ds;
      },
    }),
    UsersModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
